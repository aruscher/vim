"NERTree
map <F2> :NERDTreeToggle<CR> 

"CtrlP Search
map <F3> :CtrlP<CR>
let g:ctrlp_max_height=30
set wildignore+=*.pyc
set wildignore+=*_build/*
set wildignore+=*__pycache__/*

"Gundo Grap
map <F4> :GundoToggle<CR>

"Powerline
set guifont=DejaVu\ Sans\ Mono\ for\ Powerline\ 9
set laststatus=2
