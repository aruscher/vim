"GENERAL PLUGINS
"Vim-Powerline
Bundle 'Lokaltog/powerline',{'rtp': 'powerline/bindings/vim/'}
"Vundle Mange Vundle
Bundle 'gmarik/vundle' 
"Nerdtree File Explorer
Bundle 'scrooloose/nerdtree' 
"Surround for nicer surround handeling
Bundle 'tpope/vim-surround'
"CtrlP full path fuzzy-finder
Bundle 'kien/ctrlp.vim'
"Gundo -> Undo+Redo Graph
Bundle 'sjl/gundo.vim'
"Snipmate
Bundle 'msanders/snipmate.vim'


"PYTHON PLUGINS
"#########################
"Python Mode
"Bundle 'klen/python-mode' 

"R PLUGINS
"########################
"

"JAVA PLUGINS
"########################
"

"SCALA PLUGINS
"#######################
