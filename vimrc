"Vundle Install
set nocompatible
filetype off

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

source /home/$USER/.vim/basicconfig.vim
source /home/$USER/.vim/bundles.vim
source /home/$USER/.vim/bundleconfig.vim

filetype plugin indent on
