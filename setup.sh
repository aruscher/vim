#!/bin/sh
echo "Vim Install Script"
echo "##################"

echo ">Create .fonts"
mkdir ~/.fonts
echo "<Created .fonts"

echo ">Insert Vundle"
git clone git://github.com/gmarik/vundle.git /home/$USER/.vim/bundle/vundle
echo "<Inserted Vundle"

echo ">Insert Snipmate"
git clone git://github.com/msanders/snipmate.vim ~/.vim/bundle/snipmate.vim
echo "<Insertet Snipmate"

echo "Get patched fonts"
git clone git://github.com/Lokaltog/powerline-fonts.git /tmp/fonts
echo "Got patched fonts"

echo ">Move patched font"
cd /tmp/fonts
find -name "DejaVu Sans Mono for Powerline.ttf" -exec cp {} ~/.fonts \;
cd ~
echo "<Font Moved"

echo ">Update Font Cache"
fc-cache -vf /home/$USER/.fonts
echo "<Updated Font Cache"

echo ">Get better Snippets"
git clone git://github.com/honza/vim-snippets.git /tmp/snippets
cp /tmp/snippets/snippets/* ~/.vim/bundle/snipmate.vim/snippets/
echo "<Got better Snipptes"

echo ">Get fanzy colorschmes"
git clone git://github.com/flazz/vim-colorschemes /tmp/colors
mkdir ~/.vim/colors
cp /tmp/colors/colors/* ~/.vim/colors/
echo "<Got fanzy colorschmes"
