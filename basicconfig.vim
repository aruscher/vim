"Remap leader to ,
let mapleader = ","

"Dont write fucking backup!
set nobackup
set nowritebackup
set noswapfile

"FORCE TO LEARN THE RIGHT MOVEMENTS
inoremap  <Up>     <NOP>
inoremap  <Down>   <NOP>
inoremap  <Left>   <NOP>
inoremap  <Right>  <NOP>
noremap   <Down>   <NOP>
noremap   <Left>   <NOP>
noremap   <Right>  <NOP>
noremap   <Up>     <NOP>

"Easier move between tabs
map <Leader>tt <esc>:tabnew<CR>
map <Leader>tn <esc>:tabnext<CR>
map <Leader>tp <esc>:tabprevious<CR>
map <Leader>tc <esc>:tabclose<CR>

"Easier move between windows
map <Leader>wh :split 
map <Leader>wv :vsplit
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

"Nice sort-function
vnoremap <Leader>s :sort<CR>

"ColorScheme
set t_Co=256
set background=dark
colorscheme wombat256

"Syntax
filetype off
filetype plugin indent on
syntax on

"Show line numbers
set number
set tw=79
set nowrap
set colorcolumn=80
highlight ColorColumn ctermbg=233

"Better History
set history=700
set undolevels=700

"Spaces are better than Tabs
set tabstop=4
set softtabstop=4
set shiftwidth=4
set shiftround
set expandtab

"Better Identation
vmap > >gv
vmap < <gv
